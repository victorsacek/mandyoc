# Project Authors

MANDYOC is being developed and currently maintained by

- [Victor Sacek](https://bitbucket.org/victorsacek): mail -
IAG, Universidad de São Paulo, Brazil - (ORCID: number)

The following people have made significantly contributions to the project
(in alphabetical order by last name) are considered "The MANDYOC Developers":

- [developers_name](web): mail - Affiliation - (ORCID: number)
